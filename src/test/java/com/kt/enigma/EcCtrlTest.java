
package com.kt.enigma;

import com.google.gson.Gson;
import com.kt.enigma.custom.ECKafkaUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import vo.CnvyRow;
import vo.ItgCnvyData;
import vo.KafkaMsgType;
import vo.SpotDevCnvyData;
import vo.row.ContlData;
import vo.row.StrData;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application.properties")
public class EcCtrlTest {


    @Value("${sdk.serverIp}")
    String serverIp;

    @Value("${sdk.serverPort}")
    String serverPort;

    @Value("${sdk.externalSysId}")
    String externalSysId;

    @Value("${sdk.deviceId}")
    String deviceId;

    @Value("${sdk.password}")
    String password;

    @Value("${ktmeg.svcTgtSeq}")
    long svcTgtSeq;

    @Value("${ktmeg.spotDevSeq}")
    long spotDevSeq;


    @Value("${ktmeg.ecSrvrId}")
    String ecSrvrId;


    @Value("${ktmeg.unitSvcCd}")
    String unitSvcCd;

    @Value("${ktmeg.brokerServerList}")
    String brokerServerList;


    @Value("${ktmeg.controlTopic}")
    String controlTopic;


    /* EC 제어 명령 테스트 코드 (Kafka) */
    @Test
    public void testKafkaCtrl() throws Exception {


        ECKafkaUtil ecKafkaUtil = new ECKafkaUtil();



        SpotDevCnvyData requestDevice = new SpotDevCnvyData();
        requestDevice.setSvcTgtSeq(svcTgtSeq);
        requestDevice.setSpotDevSeq(spotDevSeq);
        requestDevice.setSpotDevId(deviceId);
        requestDevice.setGwCnctId(externalSysId);

        List<CnvyRow> rows = new ArrayList<CnvyRow>();

        CnvyRow cnvyRow = new CnvyRow();

        ContlData contlData = new ContlData();
        contlData.setSnsnTagCd("1111111");
        contlData.setContlVal(1111111.0);


        List<ContlData> contlDataList = new ArrayList<>();
        contlDataList.add(contlData);

        cnvyRow.setContlDatas(contlDataList);
        CnvyRow row = new CnvyRow();


//        // 숫자형 측정
//        MsrData msrData = new MsrData();
//        msrData.setSnsnTagCd("Key");
//        //예외처리해야함
//        msrData.setRlNumVal(Double.parseDouble("0.0"));
//
//        List<MsrData> msrDatas = new ArrayList<MsrData>();
//        msrDatas.add(msrData);
//
//        row.setMsrDatas(msrDatas);

//        // 문자형 제어
        StrData strData = new StrData();
        strData.setSnsnTagCd("MSR_DT");
        strData.setStrVal("20010101");
////
        List<StrData> strDatas = new ArrayList<StrData>();
        strDatas.add(strData);

        row.setStrDatas(strDatas);

        rows.add(row);
        requestDevice.setCnvyRows(rows);
        List<SpotDevCnvyData> requestDevices = new ArrayList<SpotDevCnvyData>();
        requestDevices.add(requestDevice);

        // 제어 메시지
        ItgCnvyData request = new ItgCnvyData();
        request.setTransacId(ecKafkaUtil.createTransaction());
        request.setUnitSvcCd(unitSvcCd);
        request.setReqApiSrvrId("Service");
        request.setReqEcSrvrId(ecSrvrId);
        request.setGwCnctId(externalSysId);
        request.setExtrSysAthnId("athnRqtNo");
        request.setExtrSysAthnNo("authNo");
        request.setSpotDevCnvyDatas(requestDevices);

        byte[] packet = ecKafkaUtil.toMessage(KafkaMsgType.CONTL_ITGCNVY_DATA, new Gson().toJson(request));


        ecKafkaUtil.sendCtrlMsg(brokerServerList, controlTopic, packet);


        Assert.assertTrue(true);
    }
}