package vo.row;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 실수값 리스트 클래스
 * @since	: 2015. 10. 29.
 * @author	: CBJ
 * <PRE>
 * Revision History
 * ----------------------------------------------------
 * 2015. 10. 29. CBJ: 최초작성
 * ----------------------------------------------------
 * </PRE>
 */
public class RlNumListData implements Serializable, Cloneable
{
	/** 직렬화아이디 */
	private static final long serialVersionUID = 1552149464634620398L;

	/** 센싱태그코드 */
	private String snsnTagCd;
	/** 실수리스트값 */
	private List<Double> rlNumVals = new ArrayList<Double>();

	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
	}

	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}

	public String getSnsnTagCd() {
		return snsnTagCd;
	}

	public void setSnsnTagCd(String snsnTagCd) {
		this.snsnTagCd = snsnTagCd;
	}

	public List<Double> getRlNumVals() {
		return rlNumVals;
	}

	public void setRlNumVals(List<Double> rlNumVals) {
		this.rlNumVals = rlNumVals;
	}

}
