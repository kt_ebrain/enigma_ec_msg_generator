package vo.row;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 실수값 클래스
 * @since	: 2015. 4. 11.
 * @author	: CBJ
 * <PRE>
 * Revision History
 * ----------------------------------------------------
 * 2015. 4. 11. CBJ: 최초작성
 * ----------------------------------------------------
 * </PRE>
 */
public class RlNumData implements Serializable, Cloneable
{
	/** 직렬화아이디 */
	private static final long serialVersionUID = 1552149464634620398L;

	/** 센싱태그코드 */
	private String snsnTagCd;
	/** 실수값 */
	private Double rlNumVal;

	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
	}

	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}

	public String getSnsnTagCd() {
		return snsnTagCd;
	}

	public void setSnsnTagCd(String snsnTagCd) {
		this.snsnTagCd = snsnTagCd;
	}

	public Double getRlNumVal() {
		return rlNumVal;
	}

	public void setRlNumVal(Double rlNumVal) {
		this.rlNumVal = rlNumVal;
	}

}
