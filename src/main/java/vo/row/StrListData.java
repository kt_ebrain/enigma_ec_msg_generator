package vo.row;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;



/**
 * 문자열리스트데이터 클래스
 * @since	: 2015. 10. 29.
 * @author	: CBJ
 * <PRE>
 * Revision History
 * ----------------------------------------------------
 * 2015. 10. 29. CBJ: 최초작성
 * ----------------------------------------------------
 * </PRE>
 */
public class StrListData implements Serializable, Cloneable
{
	/** 직렬화데이터 */
	private static final long serialVersionUID = -3786632152709193405L;

	/** 센싱태그코드 */
	private String snsnTagCd;
	/** 문자열값 */
	private List<String> strVals = new ArrayList<String>();

	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
	}

	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}

	public String getSnsnTagCd() {
		return snsnTagCd;
	}

	public void setSnsnTagCd(String snsnTagCd) {
		this.snsnTagCd = snsnTagCd;
	}

	public List<String> getStrVals() {
		return strVals;
	}

	public void setStrVals(List<String> strVals) {
		this.strVals = strVals;
	}

}
