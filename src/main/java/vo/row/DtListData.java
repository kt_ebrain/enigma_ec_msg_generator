package vo.row;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 일시리스트데이터
 * @since	: 2015. 10. 29.
 * @author	: CBJ
 * <PRE>
 * Revision History
 * ----------------------------------------------------
 * 2015. 10. 29. CBJ: 최초작성
 * ----------------------------------------------------
 * </PRE>
 */
public class DtListData implements Serializable, Cloneable
{
	/** 직렬화데이터 */
	private static final long serialVersionUID = -8352346576231337794L;
	/** 센싱태그코드 */
	private String snsnTagCd;
	/** 일시리스트값 */
	private List<Date> dtVals = new ArrayList<Date>();

	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
	}

	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}

	public String getSnsnTagCd() {
		return snsnTagCd;
	}

	public void setSnsnTagCd(String snsnTagCd) {
		this.snsnTagCd = snsnTagCd;
	}

	public List<Date> getDtVals() {
		return dtVals;
	}

	public void setDtVals(List<Date> dtVals) {
		this.dtVals = dtVals;
	}

}