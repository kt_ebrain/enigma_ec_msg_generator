package vo.row;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 바이너리리스트데이터 클래스
 * @since	: 2015. 10. 29.
 * @author	: CBJ
 * <PRE>
 * Revision History
 * ----------------------------------------------------
 * 2015. 10. 29. CBJ: 최초작성
 * ----------------------------------------------------
 * </PRE>
 */
public class BinListData implements Serializable, Cloneable
{
	/** 직렬화아이디 */
	private static final long serialVersionUID = -8672018255830981890L;

	/** 센싱태그코드 */
	private String snsnTagCd;
	/** 이진값리스트내역 */
	private List<byte[]> binValTxns = new ArrayList<byte[]>(); ;

	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
	}

	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}

	public String getSnsnTagCd() {
		return snsnTagCd;
	}

	public void setSnsnTagCd(String snsnTagCd) {
		this.snsnTagCd = snsnTagCd;
	}

	public List<byte[]> getBinValTxns() {
		return binValTxns;
	}

	public void setBinValTxns(List<byte[]> binValTxns) {
		this.binValTxns = binValTxns;
	}

}
