package vo.row;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class BinSetupDataInExternal implements Serializable, Cloneable
{
	private String snsnTagCd;
	//private byte[] setupVal;
	private Object setupVal;


	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
	}

	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}

	public final String getSnsnTagCd() {
		return snsnTagCd;
	}

	public final void setSnsnTagCd(String snsnTagCd) {
		this.snsnTagCd = snsnTagCd;
	}

	public final Object getSetupVal() {
		return setupVal;
	}

	public final void setSetupVal(Object setupVal) {
		this.setupVal = setupVal;
	}


}
