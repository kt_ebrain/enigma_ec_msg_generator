package vo.row;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;


/**
 * 정수값
 * @since	: 2015. 4. 11.
 * @author	: CBJ
 * <PRE>
 * Revision History
 * ----------------------------------------------------
 * 2015. 4. 11. CBJ: 최초작성
 * ----------------------------------------------------
 * </PRE>
 */
public class IntData implements Serializable, Cloneable
{
	/** 직렬화아이디 */
	private static final long serialVersionUID = 7982989930012600565L;
	/** 센싱태그코드 */
	private String snsnTagCd;
	/** 정수값 */
	private Integer intVal;

	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
	}

	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}

	public String getSnsnTagCd() {
		return snsnTagCd;
	}

	public void setSnsnTagCd(String snsnTagCd) {
		this.snsnTagCd = snsnTagCd;
	}

	public Integer getIntVal() {
		return intVal;
	}

	public void setIntVal(Integer intVal) {
		this.intVal = intVal;
	}


}
