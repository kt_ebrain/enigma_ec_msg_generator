package vo.row;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 정수리스트값
 * @since	: 2015. 10. 29.
 * @author	: CBJ
 * <PRE>
 * Revision History
 * ----------------------------------------------------
 * 2015. 10. 29. CBJ: 최초작성
 * ----------------------------------------------------
 * </PRE>
 */
public class IntListData implements Serializable, Cloneable
{
	/** 직렬화아이디 */
	private static final long serialVersionUID = 7982989930012600565L;
	/** 센싱태그코드 */
	private String snsnTagCd;
	/** 정수리스트값 */
	private List<Integer> intVals= new ArrayList<Integer>();

	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SIMPLE_STYLE);
	}

	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}

	public String getSnsnTagCd() {
		return snsnTagCd;
	}

	public void setSnsnTagCd(String snsnTagCd) {
		this.snsnTagCd = snsnTagCd;
	}

	public List<Integer> getIntVals() {
		return intVals;
	}

	public void setIntVals(List<Integer> intVals) {
		this.intVals = intVals;
	}


}
