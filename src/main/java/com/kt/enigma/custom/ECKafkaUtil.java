package com.kt.enigma.custom;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import vo.KafkaEncdngType;
import vo.KafkaMsgType;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;

@Component
public class ECKafkaUtil {

    // Domain Version
    public static String version = "2.0";

    private static final Logger logger = LoggerFactory.getLogger(ECKafkaUtil.class);


    public void sendCtrlMsg(String brokerServerList, String controlTopic, byte[] message) throws Exception {

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        Gson gson = gsonBuilder.create();

        Properties props = new Properties();
        // TODO Broker Address. This is sample
        String broker = brokerServerList;
        props.put("metadata.broker.list", broker);
        props.put("serializer.class", "kafka.serializer.DefaultEncoder");
        props.put("partitioner.class", "kafka.producer.DefaultPartitioner");
        props.put("request.required.acks", "0");
        Producer<String, byte[]> producer = new Producer<String, byte[]>(new ProducerConfig(props));

        try{
            KeyedMessage<String, byte[]> data = new KeyedMessage<String, byte[]>(controlTopic, message);
            producer.send(data);

        }catch (Exception e){

            e.printStackTrace();
        }
    }

    public byte[] toMessage(KafkaMsgType messageType, String messageBody
    ) throws Exception {

        short type = messageType.getValue();
        int length = messageBody.length();

        /** header 인코딩타입(1byte)+reserved(1byte)+메세지유형(2byte)+바디길이(4byte) */
        byte[] header = new byte[8];
        header[0] = KafkaEncdngType.JSON.getValue();
        header[1] = 0;

        header[2] = (byte) ((type & 0xff00) >> 8);
        header[3] = (byte) (type & 0xff);

        header[4] = (byte) ((length >> 24) & 0xff);
        header[5] = (byte) ((length >> 16) & 0xff);
        header[6] = (byte) ((length >> 8) & 0xff);
        header[7] = (byte) ((length) & 0xff);

        StringBuilder log = new StringBuilder();
        log.append("*************************Send JSON Message*******************\n");
        log.append("header ::{encodingType:" + KafkaEncdngType.JSON.getValue() + ",reserved:" + 0 + ",msgType:" + type
                + ",bodySize:" + length + "} ");
        log.append("body ::" + messageBody);
        log.append("\n*************************************************************");
        logger.info(log.toString());

        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        try {
            stream.write(header);
            stream.write(messageBody.getBytes());

            byte[] message = stream.toByteArray();
            return message;

        } catch (IOException e) {
            throw e;
        } finally {
            IOUtils.closeQuietly(stream);
        }

    }

    public String createTransaction() {
        return String.valueOf(System.currentTimeMillis());
    }

}
