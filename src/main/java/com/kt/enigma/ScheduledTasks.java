package com.kt.enigma;


import com.google.gson.Gson;
import com.kt.smcp.gw.ca.comm.exception.SdkException;
import com.kt.smcp.gw.ca.gwfrwk.adap.stdsys.sdk.tcp.IMTcpConnector;
import com.kt.smcp.gw.ca.util.IMUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

//import com.kt.smcp.gw.ca.gwfrwk.adap.stdsys.sdk.tcp.IMTcpConnector;

//import com.kt.smcp.gw.ca.gwfrwk.adap.stdsys.sdk.tcp.IMTcpConnector;

@Component
public class ScheduledTasks {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    IMTcpConnector imTcpConnector;


    // 스케줄 조정 부분
//    @Scheduled(fixedDelay = 5000, initialDelay = 10000)
    public void sendDummyData() {
        try {
            Long transID = IMUtil.getTransactionLongRoundKey4();
            Map<String, String> msrDTMap = new HashMap<String, String>();
            String nowStr = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());


            msrDTMap.put("MSR_DT", nowStr);
//            imTcpConnector.requestColecDatas(generateDataForEMO(), msrDTMap, new Date(), transID);
            imTcpConnector.requestColecDatas(null, generateData(), new Date(), transID);

        } catch (SdkException e) {
            // 연결 해제
            imTcpConnector.disconnect();
            // 메모리 해제
            imTcpConnector.destroy();
            logger.warn("Code :" + e.getCode() + " Message :" + e.getMessage());
        }

    }

    // for EMO-TB
    public Map<String, Double> generateDataForEMO() {


        Map<String, Double> sampleMap = new HashMap<>();

        for (int i = 0; i < 30; i++) {
            double randomNum = ThreadLocalRandom.current().nextDouble(10, 20 + 1);

            if (i < 10) {
                sampleMap.put("000000" + i, randomNum);

            } else {
                sampleMap.put("00000" + i, randomNum);

            }

        }
        return sampleMap;
    }

    // for P-Hotel
    public Map<String, String> generateData() {


        Map<String, Object> sampleMap = new HashMap<>();

        for (int i = 0; i < 1000; i++) {
            double randomNum = ThreadLocalRandom.current().nextDouble(10, 20 + 1);

            sampleMap.put("TAG_TAG_TAG_TAG_000000" + i, randomNum);


        }
        sampleMap.put("MSR_DT", "20180711.211111");
        String json = new Gson().toJson(sampleMap);

        Map<String, String> dummyMap = new HashMap<>();
        dummyMap.put("raw_data", json);
        logger.info("dummy Map :" + dummyMap.toString());

        return dummyMap;
    }
//
////    @Scheduled(fixedRate = 5000)
//    public void sendMsg() {
//
//
//        CollectEvent collectEvent = new CollectEvent();
//        collectEvent.setSvcTgtSeq((long)1);
//        collectEvent.setSpotDevSeq(Long.parseLong("31580000001"));
//        collectEvent.setDeviceModelId("MEMS3158");
//        collectEvent.setOccDt("222");
//        sendECUtil.sendEC(collectEvent,"","ss");
//
//    }
}