package com.kt.enigma;

import com.kt.smcp.gw.ca.gwfrwk.adap.stdsys.sdk.tcp.IMCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class MegCallback extends IMCallback {
    private final Logger logger = LoggerFactory.getLogger(MsgTesterApp.class);


    @Override
    public void handleColecRes(Long aLong, String s) {


        logger.info("handleColecRes :" + s);
    }

    @Override
    public void handleControlReq(Long aLong, Map<String, Double> map, Map<String, String> map1) {
        logger.info("======================ControlReq :" + map.toString() + map1.toString());


    }
}
