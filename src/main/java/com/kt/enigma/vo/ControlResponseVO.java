package com.kt.enigma.vo;

import java.util.HashMap;

public class ControlResponseVO {

	String CONTROLLDT;
	HashMap<String, Object> data = new HashMap<String, Object>();
	
	public String getCONTROLLDT() {
		return CONTROLLDT;
	}
	public void setCONTROLLDT(String controlDt) {
		this.CONTROLLDT = controlDt;
	}
	public HashMap<String, Object> getData() {
		return data;
	}
	public void setData(HashMap<String, Object> data) {
		this.data = data;
	}

}
