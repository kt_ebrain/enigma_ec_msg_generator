package com.kt.enigma;

//import com.kt.smcp.gw.ca.comm.exception.SdkException;

//import com.kt.smcp.gw.ca.comm.exception.SdkException;
//import com.kt.smcp.gw.ca.gwfrwk.adap.stdsys.sdk.tcp.BaseInfo;
//import com.kt.smcp.gw.ca.gwfrwk.adap.stdsys.sdk.tcp.IMTcpConnector;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.context.annotation.Bean;
//import org.springframework.scheduling.annotation.EnableScheduling;

//import com.kt.smcp.gw.ca.gwfrwk.adap.stdsys.sdk.tcp.BaseInfo;

//import com.kt.smcp.gw.ca.gwfrwk.adap.stdsys.sdk.tcp.IMTcpConnector;

import com.kt.smcp.gw.ca.comm.exception.SdkException;
import com.kt.smcp.gw.ca.gwfrwk.adap.stdsys.sdk.tcp.BaseInfo;
import com.kt.smcp.gw.ca.gwfrwk.adap.stdsys.sdk.tcp.IMTcpConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MsgTesterApp {

    public static void main(String[] args) {
        SpringApplication.run(MsgTesterApp.class, args);
    }

    private final Logger logger = LoggerFactory.getLogger(MsgTesterApp.class);

    @Autowired
    MegCallback megCallback;


    @Value("${ec.server.ip}")
    String serverIp;

    @Value("${ec.server.port}")
    Integer serverPort;

    @Value("${ec.server.externalSysId}")
    String externalSysId;

    @Value("${ec.client.deviceId}")
    String deviceId;

    @Value("${ec.client.password}")
    String password;


    @Bean
    public IMTcpConnector iMTcpConnector() {

        IMTcpConnector imTcpConnector = new IMTcpConnector();
        BaseInfo baseInfo = new BaseInfo();

        Long timeOut = (long) 3000;
        try {

            // 초기화
            baseInfo.setIp(serverIp);
            baseInfo.setPort(serverPort);
            baseInfo.setExtrSysId(externalSysId);
            baseInfo.setDeviceId(deviceId);
            baseInfo.setPassword(password);

//            baseInfo = IMUtil.getBaseInfo("IoTSDK.properties");
            imTcpConnector.init(megCallback, baseInfo);
            // 연결
            imTcpConnector.connect(timeOut);
            // 인증
            imTcpConnector.authenticate(timeOut);


        } catch (SdkException e) {
            logger.warn("Code :" + e.getCode() + " Message :" + e.getMessage());
        }

        logger.info("DeviceID : " + baseInfo.getDeviceId());
        logger.info("Code :" + "OK");

        return imTcpConnector;
    }


}
