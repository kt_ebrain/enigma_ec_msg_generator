# EC Tester : 데이터 연동 테스트 모듈 for EMO
* 5초마다 dummy 데이터를 생성 -> EC로 전송 (* ScheduledTasks 클래스에서 수정 가능)

## 빌드 
```
mvn clean package
```

## 실행 
```
 java -jar target/enigma-ec-msg-generator-1.0-SNAPSHOT.jar

```

---------


## 설정
* application.properties
```
## EC Server IP #사외
sdk.serverIp=14.63.248.16
#sdk.serverIp=10.217.74.189 #사내 IP

# Server Port : 서비스 포트 정보
sdk.serverPort=9051 
# Service ID
sdk.externalSysId=EMO_IF
# Device ID : 장치아이디
sdk.deviceId=EMO111111
# Password : 인증 요청 번호
sdk.password=1234567890

```
## 데이터 확인 
ex) 수신된 메세지 
* http://14.52.244.209:8080/GWCommAgent
* Home > 모니터링 > 하위 트랜잭션 패킷 모니터링 > 게이트웨이 수신 패킷 모니터링
* 수신모듈 명
 
  EMO_IF_ITG  
